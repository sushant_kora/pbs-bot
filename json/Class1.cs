﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace json
{
    public class Class1
    {
        public void LoadJso()
        {
            using (StreamReader r = new StreamReader("PBSDefenceStr.json"))
            {
                string json = r.ReadToEnd();
                List<Item> items = JsonConvert.DeserializeObject<List<Item>>(json);
            }
        }

        public class Item
        {
            public class Child7
            {
                public string name { get; set; }
            }

            public class Child6
            {
                public string name { get; set; }
                public List<Child7> children { get; set; }
            }

            public class Child5
            {
                public string name { get; set; }
                public List<Child6> children { get; set; }
            }

            public class Child4
            {
                public string name { get; set; }
                public List<Child5> children { get; set; }
            }

            public class Child3
            {
                public string name { get; set; }
                public List<Child4> children { get; set; }
            }

            public class Child2
            {
                public string name { get; set; }
                public List<Child3> children { get; set; }
            }

            public class Child
            {
                public string name { get; set; }
                public List<Child2> children { get; set; }
            }

            public class RootObject
            {
                public string name { get; set; }
                public List<Child> children { get; set; }
            }
        }
    }
}
